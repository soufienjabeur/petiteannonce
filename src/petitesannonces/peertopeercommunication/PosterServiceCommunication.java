/**
* TP n1  :
*
* Titre du TP : TCP/UDP
* Partie : Petite Annonce
* Date :08/12/2019
*
* Nom : Jabeur
* Pr�nom : Soufien
* N� d'�tudiant : 21956753
* Groupe: K
* Bin�me: 37 
*
* email : soufien.jabeur@etu.univ-paris-diderot.fr
*
* Remarques :
*/
package petitesannonces.peertopeercommunication;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

import petitesannonces.client.Client;

public class PosterServiceCommunication extends Thread implements IPosterServiceCommunication {

	int port;
	String localhost;

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;

	}

	public PosterServiceCommunication(int port) {
		super();
		this.port = port;
		this.localhost = "localhost";
	}

	@Override
	public void communicate(Client client) {
		DatagramSocket ServeurSocket;
		try {
			System.out.println("=========== vendeur ===========");
			
			Scanner inFromUser = new Scanner(System.in);
			ServeurSocket = new DatagramSocket(client.getPeerTopeerport());
			InetAddress IPAddress = InetAddress.getByName(localhost);

			String reply, sentence;

			do {
				byte[] sendData = new byte[1024];
				byte[] receiveData = new byte[1024];
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				ServeurSocket.receive(receivePacket);
				reply = new String(receivePacket.getData());
				System.out.print("Client: ");
				System.out.println(reply);

				System.out.print("Moi: ");
				sentence = inFromUser.nextLine();
				sendData = sentence.getBytes();
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, receivePacket.getAddress(),
						receivePacket.getPort());
				ServeurSocket.send(sendPacket);

			} while (!reply.equals("bye"));
			ServeurSocket.close();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

}
