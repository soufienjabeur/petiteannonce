/**
* TP n1  :
*
* Titre du TP : TCP/UDP
* Partie : Petite Annonce
* Date :08/12/2019
*
* Nom : Jabeur
* Pr�nom : Soufien
* N� d'�tudiant : 21956753
* Groupe: K
* Bin�me: 37 
*
* email : soufien.jabeur@etu.univ-paris-diderot.fr
*
* Remarques :
*/
package petitesannonces.peertopeercommunication;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class ClientServiceCommunctaion extends Thread implements IClientServiceCommunication {
	static int port;
	static String localhost;
	final static String delims = "!!";

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public ClientServiceCommunctaion(int port, String localhost) {
		super();
		this.port = port;
		this.localhost = "localhost";
	}

	@Override
	public void communicate(String info) {
		DatagramSocket ClientSocket;
		try {
			System.out.println("======= Client =======");

			String request;
			String reply;
			Scanner inFromUser = new Scanner(System.in);
			do {
				byte[] receiveData = new byte[1024];
				byte[] sendData = new byte[1024];
				System.out.print("Moi: ");
				request = inFromUser.nextLine();
				sendData = request.getBytes();
				ClientSocket = new DatagramSocket();
				System.out.println(Integer.parseInt(info.split(delims)[info.split(delims).length - 4]));
				ClientSocket.connect(InetAddress.getByName(this.localhost),
						Integer.parseInt(info.split(delims)[info.split(delims).length - 4]));
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ClientSocket.getInetAddress(),
						ClientSocket.getPort());
				ClientSocket.send(sendPacket);

				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				ClientSocket.receive(receivePacket);
				reply = new String(receivePacket.getData());
				System.out.println(info.split(delims)[info.split(delims).length - 2] + ": " + reply);

			} while (!request.equals("bye"));
			ClientSocket.disconnect();
			ClientSocket.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
