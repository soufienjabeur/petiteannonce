/**
* TP n1  :
*
* Titre du TP : TCP/UDP
* Partie : Petite Annonce
* Date :08/12/2019
*
* Nom : Jabeur
* Pr�nom : Soufien
* N� d'�tudiant : 21956753
* Groupe: K
* Bin�me: 37 
*
* email : soufien.jabeur@etu.univ-paris-diderot.fr
*
* Remarques :
*/
package petitesannonces.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import javax.swing.plaf.synth.SynthSeparatorUI;

import petitesannonces.securite.AES;

public class AnnonceMetierImpl implements IAnnonce {

	final static String delims = "!!";
	String key;
	Client client;

	// m�thode utilis�e pour poster une nouvelle annonce
	@Override
	public void addAnnonce(String choice, ArrayList<Annonce> annonces, BufferedReader br, Scanner sc, Socket sock,
			Client client, PrintWriter pw) {
		String request = "";
		try {
			client = client;
			AES aes = new AES();
			Annonce annonce = new Annonce();
			System.out.println(
					"Veuillez choisir l'une de ces cat�gories: voiture / moto / musique / �lectrom�nager / t�l�phone / autres ");

			annonce.setCategorie(Categorie.valueOf(sc.nextLine()));
			System.out.print("Titre: ");
			annonce.setTitre(sc.nextLine());
			System.out.print("Description: ");
			annonce.setDescription(sc.nextLine());
			System.out.print("Prix:");
			annonce.setPrix(sc.nextDouble());
			client.setAdresse(sock.getLocalAddress().toString().split("/")[1]);
			client.setPort(sock.getLocalPort());
			annonce.setClient(client);
			key = client.getPassword();
			request = choice + delims + annonce.getTitre() + delims + annonce.getCategorie() + delims
					+ String.valueOf(annonce.getPrix()) + delims + annonce.getDescription() + delims
					+ client.getAdresse() + delims + client.getPort();

			pw.println(aes.encrypt(request, key));
			pw.flush();
			String confirmation;
			confirmation = aes.decrypt(br.readLine(), key);
			if (confirmation.split(delims)[0].equals("ack-add")) {
				System.out.println("L'ajout de votre annonce a �t� effectu� avec succ�s :)");
			} else {
				System.out.println("Echec de l'ajout de votre annonce :(");
			}
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	// m�thode utilis�e pour supprimer une annonce
	@Override
	public void deleteAnnonce(String choice, BufferedReader br, Scanner sc, PrintWriter pw, Client client) {

		try {
			AES aes = new AES();
			System.out.print("Identifant de l'annonce � retirer: ");
			String request = choice + delims + sc.nextLine() + delims;
			key = client.getPassword();
			pw.println(aes.encrypt(request, key));
			String reponse = aes.decrypt(br.readLine(), key);
			if (reponse.split(delims)[1].equals("1")) {
				System.out.println("La suppression de votre annonce a �t� effectu� avec succ�s :)");
			} else {
				System.out.println("Echec du supression  :(");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// m�thode utilis�e pour consulter toutes les annonces
	public void consultAllAnnonces(String choice, BufferedReader br, PrintWriter pw, Client client) {
		try {
			AES aes = new AES();
			key = client.getPassword();
			pw.println(aes.encrypt(choice + delims, key));
			String reponse = (aes.decrypt(br.readLine(), key));
			String[] replyFromServeur = reponse.split(delims);

			if (Integer.parseInt(replyFromServeur[1]) == 0) {
				System.out.println("y a pas des annonces � consulter pour l'instant");
			} else {
				System.out.println("Voila la liste des annonces !!");
				for (int i = 2; i < replyFromServeur.length; i++)
					System.out.println(replyFromServeur[i] + "\n");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// m�thode utilis�e pour consulter ces propres annonces
	@Override
	public void consultMyAnnonces(String choice, BufferedReader br, PrintWriter pw, Client client) {
		try {
			AES aes = new AES();
			key = client.getPassword();
			System.out.println(key);
			pw.println(aes.encrypt(choice + delims, key));
			String reponse = aes.decrypt(br.readLine(), key);
			String[] replyFromServeur = reponse.split(delims);
			if (Integer.parseInt(replyFromServeur[1]) == 0) {
				System.out.println("y a pas des annonces � consulter pour l'instant");
			} else {
				System.out.println("Voila la liste des annonces !!");
				for (int i = 2; i < replyFromServeur.length; i++)
					System.out.println(replyFromServeur[i] + "\n");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
