/**
* TP n1  :
*
* Titre du TP : TCP/UDP
* Partie : Petite Annonce
* Date :08/12/2019
*
* Nom : Jabeur
* Pr�nom : Soufien
* N� d'�tudiant : 21956753
* Groupe: K
* Bin�me: 37 
*
* email : soufien.jabeur@etu.univ-paris-diderot.fr
*
* Remarques :
*/
package petitesannonces.client;

import java.io.BufferedReader;

import java.io.InputStreamReader;

import java.io.PrintWriter;

import java.net.Socket;

import java.util.Scanner;

import petitesannonces.peertopeercommunication.ClientServiceCommunctaion;
import petitesannonces.peertopeercommunication.PosterServiceCommunication;
import petitesannonces.securite.AES;

public class ClientMainInterface {

	public static void main(String[] args) {
		try {
			// Demande de connexion sur la machine de port 1027 avec le scoket d��change de
			// flux sck
			// "192.168.137.122"
			Socket sck = new Socket("localhost", 1027);

			String key = "annonce";
			String password = "";
			// Instanciation d'un client et de l'interface IAnnonce
			Client client = new Client();
			IAnnonce iannonce = new AnnonceMetierImpl();
			IConnexion iConnexion = new Connexion();

			AES aes = new AES();
			// cr�ation des flux pour �changer de texte et des objets
			Scanner scanner = new Scanner(System.in);
			PrintWriter pw = new PrintWriter(sck.getOutputStream(), true);
			BufferedReader br = new BufferedReader(new InputStreamReader(sck.getInputStream(), "UTF-8"));

			// affichage de l'interface de l'application
			System.out.println("*** L'application de gestion de petites annonces vous offre les options suivantes ***");
			System.out.println("      ==========================================================================  ");
			System.out.println(

					"                    \n                 * Connexion                         * Deconnexion "
							+ "          \n                 * add-Ann                           * del-Ann      "
							+ "          \n                 * mes-Ann                           * all-Ann "
							+ "          \n                 * Contacter un posteur              * cou-cou ");
			while (true) {

				String choix = scanner.nextLine();

				switch (choix) {

				case "Connexion": {

					iConnexion.connexion(choix, br, scanner, pw, sck, client);
				}

					break;
				case "Deconnexion": {
					iConnexion.deconnexion(choix, br, scanner, pw, sck);
				}

					break;
				case "add-Ann": {

					iannonce.addAnnonce(choix, client.getAnnonces(), br, scanner, sck, client, pw);
				}

					break;
				case "del-Ann": {
					iannonce.deleteAnnonce(choix, br, scanner, pw, client);
				}
					break;
				case "mes-Ann": {

					iannonce.consultMyAnnonces(choix, br, pw, client);
				}
					break;
				case "all-Ann": {

					iannonce.consultAllAnnonces(choix, br, pw, client);

				}
					break;
				case "cou-cou": {
					new PosterServiceCommunication(0).communicate(client);
				}
				case "ack-msg": {

				}
				case "Contacter un posteur": {
					key=client.getPassword();
					System.out.println("Veuillez saisir l'identifiant de l'annonce");
					String annonceIdentifiant = scanner.nextLine();
					pw.println(aes.encrypt("snd-Msg!!" + annonceIdentifiant + "!!", key));
					String information = aes.decrypt(br.readLine(), key);
					for (int i = 0; i < information.split("!!").length; i++) {
						System.out.print(information.split("!!")[i] + " ");
					}
					System.out.println();
					new ClientServiceCommunctaion(0, "localhost").communicate(information);

				}
					break;
				default: {

					/*
					 * System.out.println(
					 * "*** L'application de gestion de petites annonces vous offre les options suivantes ***"
					 * ); System.out.println(
					 * "Vous pouvez:\n * Connexion \n * add-Ann  \n * del-Ann \n * mes-Ann \n * all-Ann \n * Contacter un posteur \n * Deconnexion"
					 * );
					 */
				}
				}
			}

		} catch (Exception e) {

			e.printStackTrace();
		}
	}
}
