/**
* TP n1  :
*
* Titre du TP : TCP/UDP
* Partie : Petite Annonce
* Date :08/12/2019
*
* Nom : Jabeur
* Pr�nom : Soufien
* N� d'�tudiant : 21956753
* Groupe: K
* Bin�me: 37 
*
* email : soufien.jabeur@etu.univ-paris-diderot.fr
*
* Remarques :
*/
package petitesannonces.client;

import java.io.Serializable;

public class Annonce implements Serializable {
	private String id;

	private Client client;
	private String titre;
	private Categorie categorie;
	private double prix;
	private String description;

	public Annonce(Client client, String titre, Categorie categorie, double prix, String description) {
		super();
		this.client = client;
		this.titre = titre;
		this.categorie = categorie;
		this.prix = prix;
		this.description = description;
	}

	public Annonce() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
