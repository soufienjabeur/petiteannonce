/**
* TP n1  :
*
* Titre du TP : TCP/UDP
* Partie : Petite Annonce
* Date :08/12/2019
*
* Nom : Jabeur
* Pr�nom : Soufien
* N� d'�tudiant : 21956753
* Groupe: K
* Bin�me: 37 
*
* email : soufien.jabeur@etu.univ-paris-diderot.fr
*
* Remarques :
*/
package petitesannonces.client;

import java.io.BufferedReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public interface IAnnonce {

	public void addAnnonce(String choice, ArrayList<Annonce> annonces, BufferedReader br, Scanner sc, Socket sock,
			Client client, PrintWriter pw);

	public void deleteAnnonce(String choice, BufferedReader br, Scanner sc, PrintWriter pw,Client client);

	public void consultMyAnnonces(String choice, BufferedReader br,PrintWriter pw,Client client);

	public void consultAllAnnonces(String choice, BufferedReader br, PrintWriter pw,Client client);
}
