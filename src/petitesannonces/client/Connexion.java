/**
* TP n1  :
*
* Titre du TP : TCP/UDP
* Partie : Petite Annonce
* Date :08/12/2019
*
* Nom : Jabeur
* Pr�nom : Soufien
* N� d'�tudiant : 21956753
* Groupe: K
* Bin�me: 37 
*
* email : soufien.jabeur@etu.univ-paris-diderot.fr
*
* Remarques :
*/

package petitesannonces.client;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.AbstractExecutorService;

import petitesannonces.securite.AES;

public class Connexion implements IConnexion {
	final static String delims = "!!";
	String key = "annonce";

	@Override
	public void connexion(String choix, BufferedReader br, Scanner sc, PrintWriter pw, Socket sck, Client client) {

		try {
			AES aes = new AES();
			
			int port_length = 4;
			System.out.print("Nom: ");
			String username = sc.nextLine();
			client.setName(username);
			System.out.print("Mot de passe: ");
			String password = sc.nextLine();
			client.setPassword(password);
			System.out.print("Port:");
			int port = sc.nextInt();
			if (port == sck.getLocalPort() || String.valueOf(port).length() != port_length) {
				System.out.println("Veuillez saisir un autre num�ro de  port!!");
			} else {
				client.setPeerTopeerport(port);
				pw.println(aes.encrypt(
						choix + delims + username + delims + password + delims + String.valueOf(port) + delims, key));
			}

			System.out.println("my key is"+key);
			String reply = aes.decrypt(br.readLine(), key);
			System.out.println("I'm here too");
			System.out.println(reply.split(delims)[0] + " : " + reply.split(delims)[2]);
			key = client.getPassword();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Override
	public void deconnexion(String choix, BufferedReader br, Scanner sc, PrintWriter pw, Socket sck) {
		AES aes = new AES();
		System.out.print("Veuillez saisir 'bye-bye':");
		try {
			String word = sc.nextLine();

			pw.println(aes.encrypt(choix + delims + word + delims, key));
			String reply = aes.decrypt(br.readLine(), key);
			System.out.println(reply);
			if (reply.split(delims)[0].equals("bye-bye")) {
				sck.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
