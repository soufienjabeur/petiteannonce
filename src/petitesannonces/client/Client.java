/**
* TP n1  :
*
* Titre du TP : TCP/UDP
* Partie : Petite Annonce
* Date :08/12/2019
*
* Nom : Jabeur
* Pr�nom : Soufien
* N� d'�tudiant : 21956753
* Groupe: K
* Bin�me: 37 
*
* email : soufien.jabeur@etu.univ-paris-diderot.fr
*
* Remarques :
*/
package petitesannonces.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Client implements Serializable {

	private ArrayList<Annonce> annonces;
	private int port;
	private int peerTopeerport;
	private String adresse;
	private String name;
	private String password;
	public Client(int port, String adresse, String name, String password) {
		super();
		this.port = port;
		this.adresse = adresse;
		this.name = name;
		this.password = password;
	}

	

	public Client() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Annonce> getAnnonces() {
		return annonces;
	}

	public void setAnnonces(ArrayList<Annonce> annonces) {
		this.annonces = annonces;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPeerTopeerport() {
		return peerTopeerport;
	}

	public void setPeerTopeerport(int peerTopeerport) {
		this.peerTopeerport = peerTopeerport;
	}

}
