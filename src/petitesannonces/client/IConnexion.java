/**
* TP n1  :
*
* Titre du TP : TCP/UDP
* Partie : Petite Annonce
* Date :08/12/2019
*
* Nom : Jabeur
* Pr�nom : Soufien
* N� d'�tudiant : 21956753
* Groupe: K
* Bin�me: 37 
*
* email : soufien.jabeur@etu.univ-paris-diderot.fr
*
* Remarques :
*/
package petitesannonces.client;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public interface IConnexion {
	public void connexion(String choix, BufferedReader br, Scanner sc, PrintWriter pw, Socket sck,Client client);

	public void deconnexion(String choix, BufferedReader br, Scanner sc, PrintWriter pw, Socket sck);
}
