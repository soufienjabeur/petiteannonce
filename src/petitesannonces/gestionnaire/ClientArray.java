/**
* TP n1  :
*
* Titre du TP : TCP/UDP
* Partie : Petite Annonce
* Date :08/12/2019
*
* Nom : Jabeur
* Pr�nom : Soufien
* N� d'�tudiant : 21956753
* Groupe: K
* Bin�me: 37 
*
* email : soufien.jabeur@etu.univ-paris-diderot.fr
*
* Remarques :
*/
package petitesannonces.gestionnaire;

import java.util.ArrayList;

import petitesannonces.client.Annonce;

public class ClientArray {
	private ArrayList<Annonce> clientlocallist;

	public ArrayList<Annonce> getClientlocallist() {
		return clientlocallist;
	}

	public void setClientlocallist(ArrayList<Annonce> clientlocallist) {
		this.clientlocallist = clientlocallist;
	}

	public ClientArray(ArrayList<Annonce> clientlocallist) {

		this.clientlocallist = clientlocallist;
	}

}
