package petitesannonces.gestionnaire;

import java.util.ArrayList;

import petitesannonces.client.Annonce;
import petitesannonces.client.Client;

public class Cookie {
	private ArrayList<Client> clientsCookies;
	

	public Cookie(ArrayList<Client> clientsCookies) {
		super();
		this.clientsCookies = clientsCookies;
	}

	public ArrayList<Client> getClientsCookies() {
		return clientsCookies;
	}

	public void setClientsCookies(ArrayList<Client> clientsCookies) {
		this.clientsCookies = clientsCookies;
	}
}
