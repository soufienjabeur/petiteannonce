/**
* TP n1  :
*
* Titre du TP : TCP/UDP
* Partie : Petite Annonce
* Date :08/12/2019
*
* Nom : Jabeur
* Pr�nom : Soufien
* N� d'�tudiant : 21956753
* Groupe: K
* Bin�me: 37 
*
* email : soufien.jabeur@etu.univ-paris-diderot.fr
*
* Remarques :
*/
package petitesannonces.gestionnaire;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import petitesannonces.client.Annonce;

public class Serveur extends Thread {
	public void run() {
		try {
			// Cr�er un socket d��coute se sur la machine de port 1027
			ServerSocket se = new ServerSocket(1027);
			ClientArray clientarray = new ClientArray(null);
			System.out.println("------ serveur s'ex�cute sur le port "+se.getLocalPort()+" ------");
			while (true) {
				clientarray.setClientlocallist(clientarray.getClientlocallist());
				// se mettre en �coute de demande de connexions sur se
				Socket sckev = se.accept();
				// cr�ation d�un nouveau thread qui assurera l��change entre le client et le
				// socket d'�change
				new Service(sckev, clientarray).start();
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Serveur().start();
	}

}
