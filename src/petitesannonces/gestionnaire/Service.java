/**
* TP n1  :
*
* Titre du TP : TCP/UDP
* Partie : Petite Annonce
* Date :08/12/2019
*
* Nom : Jabeur
* Pr�nom : Soufien
* N� d'�tudiant : 21956753
* Groupe: K
* Bin�me: 37 
*
* email : soufien.jabeur@etu.univ-paris-diderot.fr
*
* Remarques :
*/
package petitesannonces.gestionnaire;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import petitesannonces.client.Annonce;
import petitesannonces.client.Categorie;
import petitesannonces.client.Client;

import petitesannonces.securite.AES;

public class Service extends Thread {
	private Socket sckser;
	// L'ArrayList annonces stocke les annonces de chaque client
	private ArrayList<Annonce> annonces = new ArrayList<>();
	private ClientArray clientArray;

	final static String requestDelims = "!!";
	String key = "annonce";

	public Service(Socket sckser, ClientArray clientArray) {
		this.annonces = annonces;
		this.clientArray = clientArray;
		this.sckser = sckser;
	}

	public void run() {
		Client client = new Client();
		try {

			// cr�ation des flux pour �changer de texte, des objets
			BufferedReader br = new BufferedReader(new InputStreamReader(sckser.getInputStream()));
			PrintWriter pw = new PrintWriter(sckser.getOutputStream(), true);
			Scanner scanner = new Scanner(System.in);
			// Creation de l'objet InetSocketAddress pour r�cupere l'adresse ip et le num�ro
			// de port de client
			InetSocketAddress sockaddr = (InetSocketAddress) this.sckser.getRemoteSocketAddress();
			// Creation de l'objet iCommunication qui sert � �change de message avec le
			// client
			String username = "";
			int port = 0;
			AES aes = new AES();

			while (true) {
				// Lecture de requ�te de client
				String request = aes.decrypt(br.readLine(), key);
				System.out.println("request :" + request);
				String password = "";
				System.out.println("C'est qui j'ai recu " + request.split(requestDelims)[0]);
				switch (request.split(requestDelims)[0]) {
				case "Connexion": {

					String[] requestConnexion = request.split(requestDelims);

					username = requestConnexion[requestConnexion.length - 3];
					password = requestConnexion[requestConnexion.length - 2];

					port = Integer.parseInt(requestConnexion[requestConnexion.length - 1]);

					if (clientArray.getClientlocallist() != null) {
						for (int i = 0; i < clientArray.getClientlocallist().size(); i++) {
							if (clientArray.getClientlocallist().get(i).getClient().getName().equals(username)
									|| clientArray.getClientlocallist().get(i).getClient().getPort() == port) {
								pw.println(aes.encrypt("wel-com" + requestDelims + 0 + requestDelims, key));
								
							}
						}
					} else {
						pw.println(aes.encrypt(
								"wel-com" + requestDelims + 1 + requestDelims + username + " " + port + requestDelims,
								key));
						
					}
					System.out.println("Je suis l�");
					key = password;
				}

					break;
				case "Deconnexion": {

					if (request.split(requestDelims)[1].equals("bye-bye")) {
						pw.println(aes.encrypt("bye-bye" + requestDelims, key));
					} else {
						pw.println(aes.encrypt("Veuillez saisir bye-bye pour quitter l'application!!", key));
					}

				}

					break;
				case "add-Ann": {

					String[] requestParts = request.split(requestDelims);
					Annonce annonce = new Annonce();
					annonce.setId("Ann00" + (annonces.size() + 1));
					annonce.setTitre(requestParts[1]);
					annonce.setCategorie(Enum.valueOf(Categorie.class, requestParts[2]));
					annonce.setPrix(Double.parseDouble(requestParts[3]));
					annonce.setDescription(requestParts[4]);
					client.setAdresse(sockaddr.getHostName());
					client.setPort(sckser.getPort());
					System.out.println("Add: " + port);
					client.setName(username);
					client.setPassword(password);
					annonce.setClient(client);

					annonces.add(annonce);
					clientArray.setClientlocallist(annonces);
					pw.println(aes.encrypt("ack-add!!", key));
					pw.flush();

				}
					break;

				case "del-Ann": {

					if (annonces != null) {
						for (int i = 0; i < annonces.size(); i++) {
							if (this.annonces.get(i).getId().equals(request.split(requestDelims)[1])) {
								this.annonces.remove(i);
								clientArray.setClientlocallist(annonces);
								pw.println(aes.encrypt("ack-del!!1!!", key));
								break;
							}
						}
					} else {
						pw.println(aes.encrypt("ack-del!!0!!", key));
					}
				}
					break;
				case "all-Ann": {
					String annoncesToConsulte = "";
					if (clientArray.getClientlocallist() != null) {
						for (int i = 0; i < clientArray.getClientlocallist().size(); i++) {
							annoncesToConsulte = annoncesToConsulte + clientArray.getClientlocallist().get(i).getId()
									+ " " + clientArray.getClientlocallist().get(i).getTitre() + " "
									+ clientArray.getClientlocallist().get(i).getCategorie() + " " + " "
									+ clientArray.getClientlocallist().get(i).getPrix() + " "
									+ clientArray.getClientlocallist().get(i).getDescription() + " "
									+ clientArray.getClientlocallist().get(i).getClient().getName() + " "
									+ clientArray.getClientlocallist().get(i).getClient().getPort() + requestDelims;
						}

						pw.println(aes.encrypt(
								"ack-all" + requestDelims + String.valueOf(clientArray.getClientlocallist().size())
										+ requestDelims + annoncesToConsulte + requestDelims,
								key));
					} else {
						pw.println(aes.encrypt(
								"ack-all" + requestDelims + /*
															 * "Actuellement Il y a pas des annonces � consulter "
															 * String .valueOf(clientArray.getClientlocallist().size())
															 */0 + requestDelims + annoncesToConsulte + requestDelims,
								key));
					}
				}
					break;
				case "mes-Ann": {
					String annoncesToConsulte = "";
					int clientPort = 0;
					if (annonces != null) {
						for (int i = 0; i < annonces.size(); i++) {
							if (annonces.get(i).getClient().getPort() == sckser.getPort()) {
								clientPort = sckser.getPort();
							}
						}
						System.out.println("PortKaba :" + clientPort);
					}
					if (clientPort == 0 || annonces == null) {

						pw.println(aes.encrypt("ack-spe" + requestDelims + /*
																			 * String.valueOf(annonces.size())/*
																			 * "Actuellement Il y a pas des annonces � consulter"
																			 */
								0 + requestDelims + annoncesToConsulte + requestDelims, key));
					} else if (clientPort != 0 && annonces != null) {
						for (int i = 0; i < annonces.size(); i++) {

							annoncesToConsulte = annoncesToConsulte + annonces.get(i).getId() + " "
									+ annonces.get(i).getTitre() + " " + annonces.get(i).getCategorie() + " "
									+ annonces.get(i).getPrix() + " " + annonces.get(i).getDescription()
									+ requestDelims;

						}
						pw.println(aes.encrypt("ack-spe" + requestDelims + String.valueOf(annonces.size())
								+ requestDelims + annoncesToConsulte + requestDelims, key));
					}

				}
					break;
				case "snd-Msg": {

					String clientInformation = "";
					if (clientArray.getClientlocallist() != null) {
						for (int i = 0; i < clientArray.getClientlocallist().size(); i++) {
							if (clientArray.getClientlocallist().get(i).getId()
									.equals(request.split(requestDelims)[1])) {

								clientInformation = clientInformation + " "
										+ clientArray.getClientlocallist().get(i).getClient().getName() + requestDelims
										+ clientArray.getClientlocallist().get(i).getClient().getPort() + requestDelims
										+ clientArray.getClientlocallist().get(i).getClient().getAdresse()
										+ requestDelims;
							}
						}
						pw.println(aes.encrypt(
								"ack-msg" + (1) + requestDelims + clientInformation + requestDelims
										+ String.valueOf(clientArray.getClientlocallist().size()) + requestDelims,
								key));
					} else {
						pw.println(aes.encrypt(
								"ack-msg" + requestDelims + "Cette annonce n'existe pas " + requestDelims
										+ String.valueOf(clientArray.getClientlocallist().size()) + requestDelims,
								key));
					}

				}
				default: {
					pw.println(
							"Veuillez choisir l'une de ces options * Connexion  * add-Ann   * del-Ann  * Consulter mes annonces  * all-Ann  * Contacter un posteur  * Deconnexion");
				}

				}

			}
		} catch (Exception e) {

			e.printStackTrace();

		}

	}

// getter et setter pour l'ArryList annonces
	public ArrayList<Annonce> getAnnonces() {
		return annonces;
	}

	public void setAnnonces(ArrayList<Annonce> annonces) {
		this.annonces = annonces;
	}
}


